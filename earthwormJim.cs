MyIni _ini = new MyIni();
int _sectionCount = 0;
const int _steelPlatesPerSection = 224;
int _totalPossibleSections = 0;

void outputToScreen(IMyTextSurface textSurface, 
    int steelPlateCount, 
    int sectionsMade, int totalPossibleSections, 
    String extraMessage) 
{
    String outputText = "Steel Plates in Storage: " + steelPlateCount.ToString() + 
        "\nSections Made: " + sectionsMade.ToString() + 
        "\n# Sections of Inventory Left: " + (steelPlateCount / _steelPlatesPerSection).ToString();

    if (totalPossibleSections > 0) {
        // Do math to figure out the loading bar size based on number possible vs number completed
        int totalBarSize = 50;
        int completedSize = totalBarSize * sectionsMade / totalPossibleSections;
        int notCompletedSize = totalBarSize - completedSize;
        String loadingBar = new String('*', completedSize);
        loadingBar += new String('-', notCompletedSize);
        outputText += "\n" + loadingBar;
    }
    
    if (extraMessage != null) {
        outputText += "\n" + extraMessage;
    }
    
    textSurface.WriteText(outputText);
}

public Program()
{
    _ini.TryParse(Storage);
    _sectionCount = _ini.Get("sections", "count").ToInt32(0);
    _totalPossibleSections = _ini.Get("sections", "totalPossible").ToInt32(0);
}

public void Save()
{
    _ini.Clear();
    _ini.Set("sections", "count", _sectionCount);
    _ini.Set("sections", "totalPossible", _sectionCount);
    Storage = _ini.ToString();
}

public void Main(string argument, UpdateType updateSource)
{

    // Get the programmable block's text surface
    IMyTextSurface textSurface = Me.GetSurface(0);
    // Get the first timer block
    IMyTimerBlock timerBlock1 = GridTerminalSystem.GetBlockWithName("Timer Block 1 - Disconnect Top") as IMyTimerBlock;
    // Get the containers by their group
    IMyBlockGroup containerGroup = GridTerminalSystem.GetBlockGroupWithName("Container Group");
    // List of containers to check
    List<IMyTerminalBlock> containers = new List<IMyTerminalBlock>();
    containerGroup.GetBlocks(containers);
    
    // Check if the restart commands were provided
    if (argument != "") {
        if (argument == "reset") {
            _sectionCount = 0;
        }
        else {
            outputToScreen(textSurface, 0, _sectionCount, _totalPossibleSections, "ERROR: provided argument not recognized, exiting");
            return;
        }
    }

    if (containerGroup == null) {
        outputToScreen(textSurface, 0, _sectionCount, _totalPossibleSections, "ERROR: Could not find the container group");
    }
    else {
        // Check to see if the inventory of the cargo containers has enough to actual run the next iteration in the worm's section
        int steelPlateCount = 0;
        // Create the item type for steel plates
        MyItemType steelPlates = new MyItemType("MyObjectBuilder_Component", "SteelPlate");
        
        foreach (IMyTerminalBlock container in containers) {
            // Check to make sure the block has an inventory
            if (!container.HasInventory) {
                continue;
            }
            // Get the number of steel plates
            MyInventoryItem? nullableItem = container.GetInventory(0).FindItem(steelPlates);
            // If the item is not null, add it to the total
            if (nullableItem != null) {
                MyInventoryItem item = (MyInventoryItem) nullableItem;
                steelPlateCount += item.Amount.ToIntSafe();
            }
        }

        // Make sure there is enough steel plates
        if (steelPlateCount > _steelPlatesPerSection) {
            timerBlock1.StartCountdown();
            outputToScreen(textSurface, steelPlateCount, _sectionCount, 
                _totalPossibleSections, "SUCCESS: Started creating section " + _sectionCount.ToString());
            _sectionCount++; 
        }
        else {
            outputToScreen(textSurface, steelPlateCount, _sectionCount, 
                _totalPossibleSections, 
                "ERROR: Not Enoung Steel Plates.  " + steelPlateCount.ToString() + "/" + _steelPlatesPerSection + " Steel Plates in Storage.");
        }
    }
}
